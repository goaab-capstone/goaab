# Dependencies and Prerequisites for the Environment:
Within gym's 'setup.py' remove open-cv python dependency of atari on line 10
> $ conda install cmake scikit-learn matplotlib swig joblib pillow pandas opencv openmpi cloudpickle scipy numpy

> $ conda install -c conda-forge argparse 

> $ conda install -c anaconda mpi4py

Clone stable-baselines directory with the following commands:
> $ git clone https://github.com/hill-a/stable-baselines && cd stable-baselines

> $ pip install -e .[docs,tests,mpi]

> $ python setup.py develop

> $ conda install tensorflow==1.15.0 

    If running on GPU:
    > $ conda install tensorflow-gpu==1.15.0

> $ git clone https://github.com/openai/gym && cd gym

> $ pip install -e .

> $ cd GOAABenv && pip install -e .

Make sure python version in evironment is 3.6.* or 3.7.*

# Installation and executing CICFlowMeter:
The CICFlowMeter is an open source tool that generates Biflows from pcap files, and extracts features from these flows.

For Linux prereq

> $ sudo apt-get install libpcap-dev

## Executing
Go to the extracted directory,enter the 'bin' folder

### linux
Open a terminal and run this command

For GUI:
> $ sudo ./CICFlowMeter

For Command line:
> $ ./cfm "inputFolder" "outputFolder"

# To Initialize and verify the Environment run:
> $ cd GOAABenv && pip install -e .

# For executing GOAAB tool in Linux command line:

usage: GOAAB.py [-h] [--pcap \<pcap file name\>] [--csv \<csv file name\>] [--labeled]
  
## For a labeled input dataset run:
> $ python GOAAB.py [--csv \<csv file name\>] [--labeled]

## For converting a '.pcap' to Flow data in '.csv' format run:
> $ python GOAAB.py  [--pcap \<pcap file name\>] [--labeled]

## For running a '.pcap' file from source to classiciation run:
> $ python GOAAB.py  [--pcap \<pcap file name\>]

Note: you cannot have a pcap run in '--labeled' mode because labeling guidelines are inconsistent accross datasets

## Citations:

[1] Communications Security Establishment (CSE) & the Canadian Institute for Cybersecurity (CIC). 
A labeled diverse and comprehensive benchmark dataset for intrusion detection.(2018). 
Accessed: 31 January 2021. [Online].
Description available at https://www.unb.ca/cic/datasets/ids-2018.html.
Datasets available at https://registry.opendata.aws/cse-cic-ids2018/.

[2] Canadian Institute for Cybersecurity (CIC).
A labeled dataset containing benign and malicious Botnet network activity. (2014).
Accessed: 30 November 2020. [Online].
Description and dataset available at https://www.unb.ca/cic/datasets/botnet.html.

[3] Canadian Institute for Cybersecurity (CIC).
A labeled dataset containing benign and malicious Darknet network activity. (2020).
Accessed: 31 January 2021. [Online].
Description and dataset available at https://www.unb.ca/cic/datasets/darknet2020.html.

[4] Canadian Institute for Cybersecurity (CIC).
A labeled dataset containing benign and malicious DDoS network activity. (2019).
Accessed: 26 March 2021. [Online].
Description and dataset available at https://www.unb.ca/cic/datasets/ddos-2019.html.

[5] Iman Sharafaldin, Arash Habibi Lashkari, Saqib Hakak, and Ali A. Ghorbani, 
"Developing Realistic Distributed Denial of Service (DDoS) Attack Dataset and Taxonomy", 
IEEE 53rd International Carnahan Conference on Security Technology, Chennai, India, 2019

[6]Habibi Lashkari, Arash & Draper Gil, Gerard & Mamun, Mohammad & Ghorbani, Ali. (2016). 
Characterization of Encrypted and VPN Traffic Using Time-Related Features. 10.5220/0005740704070414. 

[7] Open AI: Stable-baselines
A set of improved implementations of Reinforcement Learning (RL) algorithms based on OpenAI Baselines.
More info here: https://stable-baselines.readthedocs.io/en/master/

## Contact Information:
Cameron Berger 
ENS USN

camberger203@gmail.com

