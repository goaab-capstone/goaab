from gym.envs.registration import register

register(
     id='GOAABenv-v0',
     entry_point='GOAABenv.envs.GOAABenv:GOAABenv',
     max_episode_steps=2000,
     kwargs={'rows' : 5000, 'file_name' : "Botnet2014.csv"}
)

register(
     id='GOAABenv-v1',
     entry_point='GOAABenv.envs.GOAABenv:GOAABenv',
     max_episode_steps=2000,
     kwargs={'rows' : 5000, 'file_name' : "Darknet.csv"}
)
