"""
    Goal Oriented Autonomous Analysis Bot: Command Line Code: GOAAB

    Author: Cameron Berger
"""
import sys, os, subprocess
import numpy as np
import joblib
import gym
import argparse
import pandas as pd
import GOAABenv
from stable_baselines import A2C
from stable_baselines.common.vec_env import DummyVecEnv
import matplotlib.pyplot as plt
from gym import spaces
from sklearn.preprocessing import *
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Perceptron
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import plot_confusion_matrix
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier

def clean(data_frame):
    """ Replaces invalid data with 0
    Args:
        data_frame::pandas.DataFrame
            Input dataset
    Returns:
        new_data_frame::pandas.DataFrame
            Cleaned input dataset
    """
    assert isinstance(data_frame, pd.DataFrame), "data_frame needs to be a pd.DataFrame"
    new_data_frame = data_frame.replace([np.inf, -np.inf, np.nan], value=0)
    return new_data_frame

def get_dset(FILE_NAME):
    """ Given a filename, read in a datset
    Args:
        FILE_NAME::string
            Dataset filename
    Returns:
        pd.DataFrame::
            Compiled Pandas Dataframe
    """
    chunks = []
    for data_frame in pd.read_csv(FILE_NAME, chunksize=50000, low_memory=False):
        data_frame.drop(['Src Port', 'Flow ID', 'Src IP',
                        'Dst IP', 'Timestamp', 'Label.1'],
                         axis=1, errors='ignore', inplace=True)
        data_frame = clean(data_frame)
        chunks.append(data_frame)
    return pd.concat(chunks, ignore_index=True)

def get_xy(data_frame, labeled):
    """ Splits pcap data into numerical features for X and
        true class Y (if in training mode). If not in training
        mode Y[:]=0
    Args:
        data_frame::pandas.DataFrame
            Input dataset
    Returns:
        x_data::np.array
            Raw numerical features of dataset
        y_data::np.array
            True classifications for each datapoint or zeros if unclassified
        labeled::boolean
            Weather or not the provided dataset is labeled or not
    """
    x_data = data_frame.drop('Label', axis=1)
    x_data = x_data.to_numpy()
    y_data = None

    if labeled:
        y_data = data_frame['Label'].to_numpy()

    return x_data, y_data

def get_model(name):
    """ Retrieves a trained model
    Args:
        name::int
            Name corresponding to appropriate model
    Returns:
        mod::varies
            Corresponding model
    """
    dump = 'dmp_'+str(name)+'.pkl'
    mod = joblib.load(dump)
    return mod

def make_env(env_id, FILE_NAME, train):
    """ Makes an new instance of an environment for a file name and a training flag
    Args:
        env_id::[str]
            Environment Identification to be used with new datatypes
        FILE_NAME::[str]
            File name corresponding to the processed PCAP data in CSV format
        train::boolean
            train is a flag that lets our agent know if the incoming data is labeled
    Returns:
        _init::env
            Environment initializer for the vectorized environment handler
    """
    def _init():
        env = gym.make(env_id)
        env.input_fname(FILE_NAME, train)
        env.change_mode(True)
        return env
    return _init

# Uses CICFlowMeter to extract features from input PCAP file
# returns the filename of the processed data in csv format
def convert_pcap(FILE_NAME):
    """ Uses CICFlowMeter Software to extract features from input an PCAP file
    Args:
        FILE_NAME::[str]
            Name of the PCAP file
    Returns:
        csv_fname::[str]
            Name of CSV file containing the flows
    """
    print("Opening {}...".format(FILE_NAME))

    # Move .pcap file from working directory to be converted
    cwd = os.getcwd() + '/'
    pcap_loc = cwd + "CICFlowMeter-4.0/bin/FromPCAP/"
    csv_loc = cwd + "CICFlowMeter-4.0/bin/ToCSV/"
    os.rename(cwd + FILE_NAME, pcap_loc + FILE_NAME)

    csv_fname = FILE_NAME + "_Flow.csv"
    # Perform converstion
    os.chdir('CICFlowMeter-4.0/bin')
    convertCommand = "./cfm FromPCAP ToCSV"
    subprocess.call(convertCommand.split())
    os.chdir('../../')

    # Move .pcap file back to working directory
    os.rename(pcap_loc + FILE_NAME, cwd + FILE_NAME)

    file_exist = os.path.isfile(csv_loc + csv_fname)
    if file_exist == False:
        return "DNE"

    # Move .csv converstion to working directory
    os.rename(csv_loc + csv_fname, cwd + csv_fname)
    print("Saved \'"+FILE_NAME+"\' as \'"+csv_fname+"\'")

    return csv_fname

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='GOAAB')
    parser.add_argument('--pcap', metavar='<pcap file name>',
                        help ='pcap file to be parsed',
                        required=False)
    parser.add_argument('--csv', metavar='<csv file name>',
                        help ='csv file for training or testing',
                        required=False)
    parser.add_argument('--labeled',
                        action="store_true",
                        help = 'The \'--labeled\' option takes a classified dataset for training purposes')
    args = parser.parse_args()

    # Is the data labeled?
    train = args.labeled

    # get filename of pcap (if it exists)
    FILE_NAME = args.pcap

    # otherwise get the csv filename
    if FILE_NAME == None:
        FILE_NAME = args.csv
        if FILE_NAME == None:
            print('No file name provided')
            sys.exit(-1)

    # validate file
    if not os.path.isfile(FILE_NAME):
        print('"{}" does not exist'.format(FILE_NAME))
        sys.exit(-1)

    # if we are not training and we have a pcap file, process file to csv
    if args.pcap != None:
        ORIG_NAME = FILE_NAME
        FILE_NAME = convert_pcap(FILE_NAME)
        if FILE_NAME == "DNE":
            print("\nFile: \'" + ORIG_NAME + "\' is improperly formatted")
            sys.exit(-1)
        if train:
            print("\nFile: \'" + FILE_NAME + "\' exported for labeling")
            sys.exit(0)

    # get dataframe of processed data and from this get x and y features
    print("Opening {}...".format(FILE_NAME))

    env = DummyVecEnv([make_env('GOAABenv-v0', FILE_NAME, False)])
    save_dir = os.getcwd()  
    model = A2C.load(save_dir+"/GOAAB_model_100000.zip", env=env)
    info = {}
    obs = env.reset()
    N_STEPS = 20
    for step in range(N_STEPS):
        action, _ = model.predict(obs, deterministic=True)
        print("Step {}".format(step + 1))
        print("Action: ", action)
        obs, reward, done, info = env.step(action)
        print('Reward =', reward, 'Done =', done)
        if done:
            print("\nGoal reached!", "reward =", reward)
            print(info[0]['Steps Taken'])
            print(info[0]['Results'])
            break

    train = True
    df = get_dset(FILE_NAME)
    x, y = get_xy(df, train)

    pre_list = []
    pre_list = info[0]["Modules"]
    for pre in pre_list:
        pre = get_model(pre)
        x = pre.transform(x)

    clf = get_model(info[0]["Classifier"])
    y_pred = clf.predict(x)
    
    if train:
        print(df['Label'].value_counts())
        class_names = ['Normal', 'Malicious']
        titles_options = [("Raw Confusion Matrix", None),
                          ("Normalized Confusion Matrix", 'true')]

        for title, normalize in titles_options:
            disp = plot_confusion_matrix(clf, x, y, display_labels=class_names,
                                        cmap=plt.cm.Blues, normalize=normalize)
            disp.ax_.set_title(title)

            print(title)
            print(disp.confusion_matrix)

        # plt.show()
        score = balanced_accuracy_score(y, clf.predict(x))
        print("\nBalanced accuracy = "+str(score)+"\n")
    else:
        df["Prediction"] = y_pred
        print(df['Prediction'].value_counts())
        NEW_FILE_NAME = "Prediction_"+FILE_NAME
        df.to_csv(NEW_FILE_NAME, index=False)
        print("\nPredction saved in Prediction column of", NEW_FILE_NAME)

    sys.exit(0)
