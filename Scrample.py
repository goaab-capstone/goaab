import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

datasets = ["Darknet.csv"]

def clean(data_frame):
    """ Replaces invalid data with 0
    Args:
        data_frame::pandas.DataFrame
            Input dataset
    Returns:
        new_data_frame::pandas.DataFrame
            Cleaned input dataset
    """
    assert isinstance(data_frame, pd.DataFrame)
    "data_frame needs to be a pd.DataFrame"
    invalid_ind = data_frame[data_frame['Dst Port']=='Dst Port'].index
    data_frame.drop(invalid_ind, inplace=True)
    data_frame.reset_index(drop=True, inplace=True)
    return data_frame

def get_dset(file_name):
    """ Given a filename, read in a datset
    Args:
        file_name::string
            Dataset filename
    Returns:
        pd.DataFrame::
            Compiled Pandas Dataframe
    """
    i = 0
    c_list = []
    cols = None
    chunks = pd.read_csv(file_name, chunksize=50000, low_memory=False)
    for data_frame in chunks:
        data_frame = clean(data_frame)
        # data_frame = add_labels(data_frame)
        c_list.append(data_frame)
        i += 1
        if i >=10:
            break

    df = pd.concat(c_list, ignore_index=True).sample(frac=1).reset_index(drop=True)
    return df

for name in datasets:
    print(name)
    df = get_dset(name)
    print(df.dtypes)
    df.to_csv("Scrambled_"+name, chunksize=50000, index=False)

print("Part 2")
for name in datasets:
    chunks = pd.read_csv("Scrambled_"+name, chunksize=50000, low_memory=False)
    print("Scrambled "+name)
    for data_frame in chunks:
        print(data_frame['Label'].unique())    

