import argparse,os

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0,1"
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'

import tensorflow as tf
import copy
import numpy as np
import gym
import GOAABenv
from stable_baselines import A2C
from stable_baselines.common.vec_env import SubprocVecEnv

def make_env(env_id, rank):
    datasets = ['0_Clean_Friday-02-03-2018_Traffic(Botnet).csv',
                '0_Clean_Friday-16-02-2018_Traffic(DoS-Hulk-Slow).csv',
                '0_Clean_Friday-23-02-2018_TrafficFor(Bruteforce).csv',
                '0_Clean_Thursday-01-03-2018_Traffic(Infilteration).csv',
                '0_Clean_Thursday-15-02-2018_Traffic(DoS-Gold-Slow).csv',
                '0_Clean_Thursday-22-02-2018_Traffic(Bruteforce).csv',
                '0_Clean_Wednesday-14-02-2018_Traffic(Bruteforce).csv',
                '0_Clean_Wednesday-21-02-2018_Traffic(DDoS-HOIC).csv',
                '0_Clean_Wednesday-28-02-2018_Traffic(Infilteration).csv',
                '2_Clean_Friday-16-02-2018_Traffic(DoS-Hulk-Slow).csv',
                '2_Clean_Wednesday-21-02-2018_Traffic(DDoS-HOIC).csv',
                '3_Clean_Friday-16-02-2018_Traffic(DoS-Hulk-Slow).csv',
                '3_Clean_Wednesday-21-02-2018_Traffic(DDoS-HOIC).csv',
                'Botnet2014.csv',
                'Clean_DrDoS_MSSQL.csv',
                'Clean_DrDoS_NTP.csv',
                'Clean_DrDoS_SSDP.csv',
                'Clean_LDAP.csv',
                'Clean_Portmap.csv',
                'Clean_TFTP.csv',
                'Clean_UDPLag.csv',
                'Scrambled_Darknet.csv',
                'benign_0.csv',
                'benign_1.csv',
                'benign_2.csv',
                'benign_3.csv',
                'benign_4.csv',
                'benign_5.csv',
                'benign_6.csv',
                'benign_7.csv',
                'benign_8.csv',
                'benign_9.csv',
                'benign_10.csv',
                'benign_11.csv',
                'benign_12.csv',
                'benign_13.csv',
                'benign_14.csv',
                'benign_15.csv',
                'benign_16.csv',
                'benign_17.csv',
                'benign_18.csv',
                'benign_19.csv',
                'benign_20.csv',
                'benign_21.csv']

    # test_set = ['1_Clean_Friday-02-03-2018_Traffic(Botnet).csv',
    #             '1_Clean_Friday-16-02-2018_Traffic(DoS-Hulk-Slow).csv',
    #             '1_Clean_Wednesday-14-02-2018_Traffic(Bruteforce).csv',
    #             '1_Clean_Wednesday-21-02-2018_Traffic(DDoS-HOIC).csv',
    #             '1_Clean_Wednesday-28-02-2018_Traffic(Infilteration).csv',
    #             '4_Clean_Friday-16-02-2018_Traffic(DoS-Hulk-Slow).csv',
    #             '4_Clean_Wednesday-21-02-2018_Traffic(DDoS-HOIC).csv',
    #             'Clean_DrDoS_DNS.csv',
    #             'Clean_DrDoS_NetBIOS.csv',
    #             'Clean_DrDoS_SNMP.csv',
    #             'Clean_DrDoS_UDP.csv',
    #             'Clean_Syn.csv',
    #             'benign_22.csv',
    #             'benign_23.csv',
    #             'benign_24.csv',
    #             'benign_25.csv',
    #             'benign_26.csv',
    #             'benign_27.csv',
    #             'benign_28.csv',
    #             'benign_29.csv',
    #             'benign_30.csv',
    #             'benign_31.csv',
    #             'benign_32.csv',
    #             'benign_33.csv']

    def _init():
        env = gym.make(env_id)
        env.input_fname(datasets[rank-len(datasets)], False)
        env.change_mode(True)
        return env
    return _init

if __name__ == '__main__':
    # Validate Environment
    print("\n-------VALIDATING ENVIRONMENT-------")
    env_id = 'GOAABenv-v0'
    env = SubprocVecEnv([make_env(env_id, i) for i in range(44)])
    print(env.observation_space)
    print(env.action_space)

    # Train the agent
    print("\n-----------TRAINING AGENT-----------")
    model = A2C('MlpPolicy', env, gamma=1, verbose=1).learn(1000000)
    save_dir = os.getcwd()
    model.save(save_dir + "/GOAAB_model_1m")

    #loaded_model = PPO.load(save_dir + "/PPO_tutorial")
