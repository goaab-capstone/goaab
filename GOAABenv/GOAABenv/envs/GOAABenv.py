"""
    Goal Oriented Autonomous Analysis Bot: GOAAB

    Author: Cameron Berger
"""
import copy
import math
import sys
import numpy as np
import gym
import pandas as pd
import matplotlib.pyplot as plt
import joblib
import pickle
from gym import spaces
from scipy import sparse
from sklearn.preprocessing import *
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Perceptron
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import plot_confusion_matrix
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import Pipeline
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier

def fill_matrix(x, rows, col):
    """ Fills in a matrix to fit observation space
    Args:
        x::np.array
            The data we using
        rows::int
            Number of rows to fill
        col::int
            Number of columns to fill
    Returns:
        new_x::np.array
            Filled row*col Matrix
    """
    new_x = np.zeros((rows, col))
    for i in range(0, x.shape[0]):
        for j in range(0, x.shape[1]):
            new_x[i][j] = x[i][j]
    return new_x

def clean(data_frame):
    """ Replaces invalid data with 0
    Args:
        data_frame::pandas.DataFrame
            Input dataset
    Returns:
        new_data_frame::pandas.DataFrame
            Cleaned input dataset
    """
    assert isinstance(data_frame, pd.DataFrame)
    "data_frame needs to be a pd.DataFrame"
    new_data_frame = data_frame.replace([np.inf, -np.inf, np.nan], value=0)
    invalid_ind = new_data_frame[new_data_frame['Dst Port']=='Dst Port'].index
    new_data_frame.drop(invalid_ind, inplace=True)
    new_data_frame.reset_index(drop=True, inplace=True)
    return new_data_frame

def get_dset(file_name):
    """ Given a filename, read in a datset
    Args:
        file_name::string
            Dataset filename
    Returns:
        pd.DataFrame::
            Compiled Pandas Dataframe
    """
    chunks = []
    i = 0
    for data_frame in pd.read_csv(file_name, chunksize=50000, low_memory=False):
        data_frame.drop(['Src Port', 'Flow ID', 'Src IP',
                        'Dst IP', 'Timestamp', 'Label.1'],
                         axis=1, errors='ignore', inplace=True)
        data_frame = clean(data_frame)
        if i>=10:
            break
        i+=1
    chunks.append(data_frame)
    return pd.concat(chunks, ignore_index=True)

def get_xy(data_frame):
    """ Splits pcap data into numerical features for X and
        true class Y (if in training mode). If not in training
        mode Y[:]=0
    Args:
        data_frame::pandas.DataFrame
            Input dataset
    Returns:
        x_data::np.array
            Raw numerical features of dataset
        y_data::np.array
            True classifications for each datapoint or zeros if unclassified
        labeled::boolean
            Weather or not the provided dataset is labeled or not
    """
    x_data = data_frame.drop('Label', axis=1)
    x_data = x_data.to_numpy()

    y_data = data_frame['Label'].to_numpy()

    return x_data, y_data

def get_model(name):
    """ Retrieves a trained model
    Args:
        name::int
            Name corresponding to appropriate model
    Returns:
        mod::varies
            Corresponding model
    """
    dump = 'dmp_'+str(name)+'.pkl'
    mod = joblib.load(dump)
    return mod

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)

def save_module(mod, name):
    """ Saves a trained model
    Args:
        mod::varies
            Trained model
        name::int
            Name corresponding to appropriate model
    """
    dump = 'dmp_'+str(name)+'.pkl'
    joblib.dump(mod, dump)

class GOAABenv(gym.Env):
    """ Environment Object for the Goal Oriented Autonomous
        Analysis Bot
    Args:
        gym.Env::Environment object
            Which GOAAB environment version we are using
    """

    trained_pre = []
                #    ("<Trained Model Name 1>", "<plk name1>"),
                #    ("<Trained Model Name 2>", "<plk name2>"),
                #    ("<Trained Model Name 3>", "<plk name3>")]

    trained_clf = [("RFC for IDS 2018", "RFC_IDS_18"),
                   ("RFC for BOT 2014", "RFC_BOT_14"),
                   ("DTC for BOT 2014", "DTC_BOT_14"),
                   ("RFC for Darknet", "RFC_DRK")]

    preprocessors = [('Norm', Normalizer()),
                     ('Max Abs',MaxAbsScaler()),
                     ('SS', StandardScaler())]

    classifiers = [('KNN', KNeighborsClassifier()),
                   ('linSVC', SVC(kernel="linear", max_iter=1000000)),
                   ('plySVC', SVC(max_iter=1000000)),
                   ('DTC', DecisionTreeClassifier()),
                   ('DTC2', DecisionTreeClassifier(criterion='gini',
                                                   max_features=None,
                                                   min_samples_leaf=1,
                                                   min_samples_split=2,
                                                   splitter='best')),
                   ('RFC', RandomForestClassifier()),
                   ('RFC2', RandomForestClassifier(criterion='gini',
                                                   max_features='auto',
                                                   min_samples_leaf=1,
                                                   min_samples_split=2)),
                   ('MLP', MLPClassifier(alpha=1)),
                   ('ABC', AdaBoostClassifier()),
                   ('GNB', GaussianNB()),
                   ('Perc', Perceptron(max_iter=1000000)),
                   ('GBC', GradientBoostingClassifier())]

    datasets = ['1_Clean_Friday-02-03-2018_Traffic(Botnet).csv',
                '1_Clean_Friday-16-02-2018_Traffic(DoS-Hulk-Slow).csv',
                '1_Clean_Wednesday-14-02-2018_Traffic(Bruteforce).csv',
                '1_Clean_Wednesday-21-02-2018_Traffic(DDoS-HOIC).csv',
                '1_Clean_Wednesday-28-02-2018_Traffic(Infilteration).csv',"Botnet2014.csv",
                "Scrambled_Darknet.csv",
                "Clean_DrDoS_DNS.csv",
                "Clean_DrDoS_NetBIOS.csv",
                "Clean_DrDoS_SNMP.csv",
                "Clean_DrDoS_UDP.csv",
                "Clean_Portmap.csv",
                "Clean_TFTP.csv",
                "Clean_DrDoS_MSSQL.csv",
                "Clean_DrDoS_NTP.csv",
                "Clean_DrDoS_SSDP.csv",
                "Clean_LDAP.csv",
                "Clean_Syn.csv", 
                "Clean_UDPLag.csv"]

    def __init__(self, rows, file_name):
        """ GOAAB environment initializer
        Args:
            rows::int
                Number of rows to use in observation space
            file_name::[str]
                File name of the relevant dataset
        """
        super(GOAABenv, self).__init__()
        self.rows = rows
        self.file_name = file_name
        self.data_frame = get_dset(file_name)
        self.pre_list= []
        self.title = ''
        self.results = ''
        self.reward = 0
        self.num_taken = 0
        self.num_cl = 0
        self.num_col = 100
        self.training = True
        self.labeled = True
        n_actions = len(self.preprocessors)+len(self.classifiers)+len(self.trained_pre)+len(self.trained_clf)
        self.dsets = load_obj("ref_datasets")
        # for i in range(len(self.datasets)):
        #     self.dsets[self.datasets[i]] = get_dset(self.datasets[i])
        # [Module, Dimensionality Reduction, Reference Dataset, use self or ref dset]
        self.action_space = spaces.MultiDiscrete([n_actions, 48, len(self.datasets), 2])
        self.full_x, self.full_y = get_xy(self.data_frame)

        self.num_feat = self.full_x.shape[1]
        self.observation_space = spaces.Box(low=-1*np.inf,
                                            high=np.inf,
                                            shape=(self.rows, self.num_col))

    def reset(self):
        """ Reset observation space and put agent at start
            state with randomly chosen raw data
        Returns:
            agent_state::np.array
                Observation space
        """
        self.num_taken = 0
        self.title = "Sample size = "+str(self.rows)+"x"+str(self.num_feat)+"\n\n"
        self.results = ''
        self.reward = 0
        self.num_cl = 0
        self.pre_list = []
        self.full_x, self.full_y = get_xy(self.data_frame)
        self.num_feat = self.full_x.shape[1]
        # Sample of the datset serves as the agent's observation
        self.ss = 5000
        if self.full_x.shape[0] < 5000:
            self.ss = self.full_x.shape[0]
        random_indices = np.random.choice(self.full_x.shape[0], size=self.ss, replace=False)
        self.agent_state = fill_matrix(self.full_x[random_indices, :], self.rows, self.num_col)
        return self.agent_state

    def step(self, action):
        """ Given an action, run the chosen module on the observation
            and determine if we have reached out goal (5 classification runs)
        Args:
            action::np.array
                action[0] contains the module of choice
                action[1] contains the number of dimensionality reduction feat
        Returns:
            agent_state::np.array
                Data (observation)
            reward::float
                Reward for the chosen action
            done::boolean
                Has the agent reached the goal
            info::dict
                Dictionary containing cumulative actions of an agent,
                results of a given classification, and saved module names
        """
        self.reward = -0.01
        self.num_taken += 1
        info = {"Classifier": "test_clf"}
        done = False
        clf_x = self.full_x
        clf_y = self.full_y
        test_clf = None
        act = action[0]
        dim_red = action[1]+2
        mine_or_ref = action[3]
        ref_name = self.datasets[action[2]]
        cl_used = False
        transformer = None

        if act >= 0 and act < len(self.trained_pre) and len(self.trained_pre)!=0:
            index = act
            self.title += str(self.num_taken)+": Pre-Processing Using Pre-Trained Model: "+self.trained_pre[index][0]+"\n"
            # OPTION 1: Apply a pre-trained pre-processor to the input
            transformer = get_model(self.trained_pre[index][1])
            self.full_x = transformer.transform(self.full_x)
            self.pre_list.append(transformer)
        elif act < len(self.trained_pre)+len(self.trained_clf):
            index = act-len(self.trained_pre)
            self.title += str(self.num_taken)+": Classification Using Pre-Trained Model: "+self.trained_clf[index][0]+"\n"
            # OPTION 2: Predict the input using a pre-trained classifier
            clf = get_model(self.trained_clf[index][1])
            test_clf = clf
            cl_used = True

        elif act < len(self.trained_pre)+len(self.trained_clf)+len(self.preprocessors):
            index = act-(len(self.trained_pre)+len(self.trained_clf))
            self.title += str(self.num_taken)+": Pre-Processing Using "+self.preprocessors[index][0]+"\n"
            if mine_or_ref == 0:
                self.title += "   Transforming with input\n"
                # OPTION 3-L : Train and apply a pre-processor using the input dataset
                transformer = self.preprocessors[index][1]
                self.full_x = transformer.fit_transform(self.full_x)
                self.pre_list.append(transformer)
            else:
                self.title += "   Transforming with input and "+ref_name+"\n"
                # OPTION 4-L : Train a pre-processor using the (reference dataset + input dataset) and apply to input
                ref_x, ref_y = get_xy(self.dsets[ref_name])
                for trans in self.pre_list:
                    ref_x = trans.fit_transform(ref_x)
                stacked_x = np.concatenate((ref_x, self.full_x))
                transformer = self.preprocessors[index][1]
                transformer.fit(stacked_x)
                self.full_x = transformer.transform(self.full_x)
                self.pre_list.append(transformer)

        elif act < len(self.trained_pre)+len(self.trained_clf)+len(self.preprocessors)+len(self.classifiers):
            index = act-(len(self.trained_pre)+len(self.trained_clf)+len(self.preprocessors))
            self.title += str(self.num_taken)+": Classification Using "+self.classifiers[index][0]+"\n"

            if self.labeled:
                if mine_or_ref == 0:
                    self.title += "   Labeled: Training with input\n"
                    # OPTION 5-L : Train a classifier using the input train-set then predict the input test-set
                    x_train, x_test, y_train, y_test = train_test_split(self.full_x, self.full_y, test_size=.30)
                    clf = self.classifiers[index][1]
                    clf.fit(x_train, y_train)
                    clf_x = x_test
                    clf_y = y_test
                    test_clf = clf
                else:
                    self.title += "   Labeled: Training with input and "+ref_name+"\n"
                    # OPTION 6-L : Train a classifier using the (reference dataset + input train-set) then predict the input test-set
                    ref_x, ref_y = get_xy(self.dsets[ref_name])
                    for trans in self.pre_list:
                        ref_x = trans.fit_transform(ref_x)
                    x_train, x_test, y_train, y_test = train_test_split(self.full_x, self.full_y, test_size=0.5)
                    stacked_x = np.concatenate((ref_x, x_train))
                    stacked_y = np.concatenate((ref_y, y_train))
                    clf = self.classifiers[index][1]
                    clf.fit(stacked_x, stacked_y)
                    clf_y = y_test
                    clf_x = x_test
                    test_clf = clf
            else:
                # OPTION 6-UL: Train a classifier using the reference dataset then predict the input
                self.title += "    Training with "+ref_name+"\n"
                ref_x, ref_y = get_xy(self.dsets[ref_name])
                for trans in self.pre_list:
                    ref_x = trans.fit_transform(ref_x)
                clf = self.classifiers[index][1]
                clf.fit(ref_x, ref_y)
                test_clf = clf
            cl_used = True
        else:
            raise ValueError("Received invalid action={} which is not part of the action space".format(action))

        # Reward function
        if cl_used: # Classifier has been used
            self.num_cl += 1
            self.results = ''
            y_pred = test_clf.predict(clf_x)
            if self.labeled or self.training:
                self.reward = balanced_accuracy_score(clf_y, y_pred)
                self.results += "\nBalanced accuracy = "+str(self.reward)+"\n"
            else:
                self.reward = 1
                
            if self.num_cl == 2:
                done = True
                save_module(test_clf, "test_clf")
                pre_names = []
                for i in range(len(self.pre_list)):
                    name = str(i)+"_pre"
                    save_module(self.pre_list[i], name)
                    pre_names.append(name)
                
                info["Modules"] = copy.copy(pre_names)

        info["Steps Taken"] = copy.copy(self.title)
        info["Results"] = copy.copy(self.results)
        
        random_indices = np.random.choice(self.full_x.shape[0], size=self.ss, replace=False)
        self.agent_state = fill_matrix(self.full_x[random_indices, :], self.rows, self.num_col)
        return self.agent_state, self.reward, done, info

    def render(self, mode='console'):
        """ Rendering of the current observation
        Args:
            mode::[str]
                Rendering of choice
        """
        print(self.title)
        print(self.results)
        return

    def close(self):
        """ Does nothing, implemented to create an environment
            based on the documentation
        """
        pass

    def input_fname(self, fname, label):
        """ Retrieves a file_name from the user for use in the environment
        Args:
            fname::[str]
                The filename that will represent the data observation
        """
        self.file_name = fname
        self.data_frame = get_dset(fname)
        self.reset()
        self.labeled = label

    def change_mode(self, mode):
        self.training = mode
